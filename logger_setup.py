import logging

from constants import ModuleConfig


def initialize_logger(module_name=ModuleConfig.MODULE_NAME):
    logger = logging.getLogger(module_name)
    logger.setLevel(logging.DEBUG)

    console_handler = logging.StreamHandler()
    console_handler.setLevel(logging.DEBUG)
    console_formatter = logging.Formatter(fmt='"%(asctime)s","%(levelname)s","%(message)s"')

    console_handler.setFormatter(console_formatter)
    logger.addHandler(console_handler)

    file_handler = logging.FileHandler('./sot_trade_routes.log')
    file_handler.setLevel(logging.DEBUG)
    file_formatter = logging.Formatter(fmt='"%(asctime)s","%(levelname)s","%(message)s"')

    file_handler.setFormatter(file_formatter)
    logger.addHandler(file_handler)
