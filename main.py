import logger_setup
from comodity_scraper.scraper_main import run_scraper

if __name__ == "__main__":
    logger_setup.initialize_logger()

    run_scraper()
