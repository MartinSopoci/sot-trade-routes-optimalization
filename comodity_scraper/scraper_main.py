import logging

from comodity_scraper import Scraper
from constants import ModuleConfig

logger = logging.getLogger(ModuleConfig.MODULE_NAME)


def run_scraper():
    my_scraper = Scraper()
    my_scraper.open_website()
    my_scraper.scrape()
    my_scraper.close()
    my_scraper.export_to_csv()
