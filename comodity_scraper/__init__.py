import logging
import math
import re
import sys
import time
from collections import defaultdict
from typing import Tuple

import yaml
from selenium import webdriver


from constants import (ModuleConfig,
                       ByCommodityNames,
                       ByPortNames
                       )

if ModuleConfig.ENVIRONMENT == 'server':
    from yaml import Loader

else:
    from yaml import CLoader as Loader


SURPLUS_REG = re.compile('• Surplus: (.+)')
SOUGHT_REG = re.compile('• Sought: (.+)')

logger = logging.getLogger(ModuleConfig.MODULE_NAME)


def new_line(line_text: str) -> Tuple[str, str, str]:
    return line_text, '', ''


def calculate_distance(first_x: int, first_y: int, second_x: int, second_y: int) -> float:
    return round(math.sqrt(math.pow((first_x - second_x), 2) + math.pow((first_y - second_y), 2)), 2)


def check_the_list(input_list: list) -> str:
    if len(input_list) != 1:
        logger.warning(f"The length of found more elements on buying and selling: {input_list}")

    return input_list[0]


class Scraper:
    def __init__(self):
        self.by_commodity = defaultdict(dict)
        self.by_port = defaultdict(dict)
        self.port_dist = defaultdict(dict)
        try:
            with open(ModuleConfig.YAML_FILE, 'r') as yaml_file:
                self.element_locations = yaml.load(yaml_file, Loader=Loader)[ModuleConfig.ENVIRONMENT]

        except Exception as e:
            logger.error(f"Yaml file: {ModuleConfig.YAML_FILE} load failed {e}")

        try:
            with open(ModuleConfig.PORT_LOCATIONS, 'r') as yaml_file:
                self.port_location = yaml.load(yaml_file, Loader=Loader)
                self._create_distance_dict()
        except Exception as e:
            logger.error(f"Yaml file: {ModuleConfig.PORT_LOCATIONS} load failed {e}")

    def close(self):
        self.driver.close()

    def _create_distance_dict(self):
        for from_port_name, from_port_position in self.port_location.items():
            for to_port_name, to_port_position in self.port_location.items():
                if from_port_name != to_port_name:
                    self.port_dist[from_port_name][to_port_name] = calculate_distance(from_port_position['x'],
                                                                                      from_port_position['y'],
                                                                                      to_port_position['x'],
                                                                                      to_port_position['y']
                                                                                      )

    def _add_trade_route(self, commodity: str, is_buying: str, is_selling: str):
        if (len(commodity) == 0) or (len(is_buying) == 0) or (len(is_selling) == 0):
            logger.warning(f"commodity: {commodity}, "
                           f"is_buying: {is_buying}, "
                           f"is_selling: {is_selling}"
                           )
        else:
            self.by_commodity[commodity] = {ByCommodityNames.BUYING: is_buying,
                                            ByCommodityNames.SELLING: is_selling
                                            }
            self.by_port[is_selling][ByPortNames.BUY] = commodity
            self.by_port[is_buying][ByPortNames.SELL] = commodity

    def _process_the_table_text(self, table_text: str):
        all_lines = table_text.split('\n')

        commodity = ''
        is_buying = ''
        is_selling = ''

        if len(all_lines) % 3 != 0:
            logger.warning(f"The scraped table text have not number of lines modular by 3. Num lines: {len(all_lines)}")

        for line_number, line_text in enumerate(all_lines):
            line_type = line_number % 3

            if line_number == 0:
                commodity, is_buying, is_selling = new_line(line_text.strip())

            else:
                if line_type == 0:
                    self._add_trade_route(commodity=commodity,
                                          is_buying=is_buying,
                                          is_selling=is_selling
                                          )

                    commodity, is_buying, is_selling = new_line(line_text.strip())

                    commodity = line_text.strip()
                elif line_type == 1:
                    is_selling = check_the_list(SURPLUS_REG.findall(line_text))

                elif line_type == 2:
                    is_buying = check_the_list(SOUGHT_REG.findall(line_text))

        self._add_trade_route(commodity=commodity,
                              is_buying=is_buying,
                              is_selling=is_selling
                              )

    def open_website(self):
        if ModuleConfig.ENVIRONMENT == 'server':
            try:
                chromeOptions = webdriver.ChromeOptions()
                chromeOptions.add_experimental_option('useAutomationExtension', False)

                self.driver = webdriver.Chrome(executable_path='/usr/lib/chromium-browser/chromedriver',
                                               chrome_options=chromeOptions,
                                               desired_capabilities=chromeOptions.to_capabilities())
                self.driver.get(self.element_locations['elements']['website'])

            except Exception as e:
                logger.error(f"Chrome driver failed to open website {e}")
                sys.exit()

        else:
            try:
                self.driver = webdriver.Chrome(ModuleConfig.CHROME_DRIVER_LOCATION)
                self.driver.get(self.element_locations['elements']['website'])

            except Exception as e:
                logger.error(f"Chrome driver failed to open website {e}")
                sys.exit()

        logger.info(f"Pause the scraper for: {self.element_locations['elements']['sleeptime']}")
        time.sleep(self.element_locations['elements']['sleeptime'])

    def scrape(self):
        table_text = self.driver.find_element_by_xpath(self.element_locations['elements']['table']).text

        self._process_the_table_text(table_text)
        logger.info(f"Successfully scraped the data. commodities: {len(self.by_commodity)} ports: {len(self.by_port)}")

    # TODO: Remove duplicity code.
    def export_to_csv(self,
                      port_file_path=ModuleConfig.PORTS_OUTPUT_NAME,
                      commodity_file_path=ModuleConfig.COMMODITY_OUTPUT_NAME
                      ):
        with open(port_file_path, 'w') as port_output_file:
            port_output_file.write(f'"{ByPortNames.NAME}",'
                                   f'"{ByPortNames.BUY}",'
                                   f'"{ByPortNames.SELL}"'
                                   f'\n')
            for port, buy_sell in self.by_port.items():
                port_output_file.write(f'"{port}",'
                                       f'"{buy_sell[ByPortNames.BUY]}",'
                                       f'"{buy_sell[ByPortNames.SELL]}"'
                                       f'\n')

        with open(commodity_file_path, 'w') as commodity_output_file:
            commodity_output_file.write(f'"{ByCommodityNames.NAME}",'
                                        f'"{ByCommodityNames.BUYING}",'
                                        f'"{ByCommodityNames.SELLING}"'
                                        f'\n')
            for commodity, buy_sell in self.by_commodity.items():
                commodity_output_file.write(f'"{commodity}",'
                                            f'"{buy_sell[ByCommodityNames.BUYING]}",'
                                            f'"{buy_sell[ByCommodityNames.SELLING]}"'
                                            f'\n')
        logger.info(f"Successfully exported to csv to port: {port_file_path}, commodity: {commodity_file_path}")