class ModuleConfig:
    ENVIRONMENT = 'local'  # 'server' #
    MODULE_NAME = 'sot_trade'
    CHROME_DRIVER_LOCATION = './chromedriver.exe'
    PORTS_OUTPUT_NAME = './ports.csv'
    COMMODITY_OUTPUT_NAME = './commodities.csv'
    YAML_FILE = './element_locations.yaml'
    PORT_LOCATIONS = './port_positions.yaml'


class ByCommodityNames:
    NAME = 'commodity'
    BUYING = 'buying'
    SELLING = 'selling'


class ByPortNames:
    NAME = 'port'
    BUY = 'buy'
    SELL = 'sell'
